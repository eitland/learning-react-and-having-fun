import { CSSTransition, TransitionGroup } from 'react-transition-group';
import React, { useState, useRef, useEffect } from 'react';
import styles from './App.module.css';
import Column from "./components/Column/Column";
import moment from 'moment';
import notes from './dummyNotes';
import MainColumn from './components/Column/MainColumn';

function App() {
  const [state, updateNotes] = useState({
    notes: notes,
    showLastColumn: false
  });

  const [columns, updateColumns] = useState([
    { id: 1, visible: false },
    { id: 2, visible: false }
  ]);

  const textareaRef = useRef(null);
  const notesEndRef = useRef(null);

  const scrollToBottom = () => {
    notesEndRef.current.scrollIntoView({ behavior: 'smooth' });
  };

  useEffect(scrollToBottom);

  const addNotePostHandler = () => {
    const updatedState = { ...state };
    updatedState.notes.push({
      id: Math.floor(100 * Math.random()),
      note: textareaRef.current.value,
      timestamp: moment()
    });
    updateNotes(updatedState);
    textareaRef.current.value = '';
    textareaRef.current.focus();
  };

  const addColumnsHandler = (columnId) => {
    const updatedColumns = columns.map((column) => {
      return {
        ...column,
        visible: (column.id === columnId) ? !column.visible : column.visible
      };
    });
    updateColumns(updatedColumns);
  };


  return (
    <div className={styles.App}>
      <div className={styles.Spacer}>{/*Horizontal spacer to the left of the columns*/}</div>
      <MainColumn
        notes={state.notes}
        notesEndRef={notesEndRef}
        textareaRef={textareaRef}
        addNotePostHandler={addNotePostHandler}
        addColumnsHandler={() => addColumnsHandler(0 + 1)} />
      <TransitionGroup component={null}>
        {columns.map((column, i) => {
          return ((column.visible) ?
            <CSSTransition key={i} timeout={500} classNames="column">
              <Column addColumnsHandler={() => addColumnsHandler(column.id + 1)} />
            </CSSTransition> : null);
        }
        )}
      </TransitionGroup>
      <div className={styles.Spacer}>{/*Horizontal spacer to the right of the columns*/}</div>
    </div>
  );
}

export default App;
