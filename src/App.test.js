import React from 'react';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event' //extra import, not included with create-react-app
import App from './App';

window.HTMLElement.prototype.scrollIntoView = function() {}; // workaround for this issue: https://stackoverflow.com/questions/53271193/typeerror-scrollintoview-is-not-a-function

test('renders at least one note', () => {
  const { getByText, } = render(<App />);
  
  const timestampElement = getByText(/Note 5 from yesterday/i); // Just get something working
  expect(timestampElement).toBeInTheDocument();
});


test('can add a note', async () => {
  const { getByTestId,getByText, getAllByText } = render(<App />);
  
  const textareaElement = getByTestId('note field'); // getByPlaceholderText also works here

  // I struggled a lot with more complicated approaches that included waits and callbacks before realizing it was this simple 
  //      |||
  //      vvv
  userEvent.type(textareaElement, 'Hello, World!'); 
  expect(textareaElement.value).toBe('Hello, World!');
  expect(getAllByText(/a few seconds ago/).length).toBe(1);

  const submitNoteButton = getByText(/Add note/);
  userEvent.click(submitNoteButton);
  expect(textareaElement.value).toBe('');
  expect(getAllByText(/a few seconds ago/).length).toBe(2);
});
