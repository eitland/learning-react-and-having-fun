import React from 'react';
import styles from './Note.module.css';
import ReactMarkdown from 'react-markdown';
import { isMoment } from 'moment';

const note = props => {
  return (
    <div className={styles.classic}>
      {<ReactMarkdown source={props.data.note} />}
      <div className={styles.footer}>
        Tagged: <i>{props.data.tags} </i>&nbsp;&nbsp; Timestamp:{' '}
        <i>
          {isMoment(props.data.timestamp)
            ? props.data.timestamp.fromNow()
            : props.data.timestamp.toString()}
        </i>
      </div>
    </div>
  );
};

export default note;
