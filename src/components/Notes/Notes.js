import React from 'react';
import Note from './Note/Note';

const notes = props => {
  return (
    <>
      {props.notes.map(note => {
        return <Note key={note.id} data={note}></Note>;
      })}
      <div ref={props.bottomRef} >&nbsp;</div>
    </>
  );
};

export default notes;