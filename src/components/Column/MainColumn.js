import React from 'react';
import Column from "./Column";
import Notes from "../Notes/Notes"
import AddNote from "../AddNote/AddNote"
import styles from "./MainColumn.module.css"

const mainColumn = (props) => {
    const columnNumber = 0;

    return (
        <Column addColumnsHandler={props.addColumnsHandler}>
            <div className={styles.Header}>
                <h1 className={styles.Heading}>Notes</h1>
            </div>

            <div className={styles.Main}>
                <Notes notes={props.notes} bottomRef={props.notesEndRef}></Notes>
            </div>
            <div className={styles.Footer}>
                <AddNote
                    textareaRef={props.textareaRef}
                    onNotePostHandler={props.addNotePostHandler} />
                <button onClick={() => props.addColumnsHandler(columnNumber + 1)}>Add column</button>
            </div>
        </Column>
    );
}

export default mainColumn;