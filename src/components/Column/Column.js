import React from "react";
import styles from "./Column.module.css";

const column = (props) => {
    return (
        <div className={styles.Column}>
            <>
                {props.children}
                <button onClick={props.addColumnsHandler}>Add column</button>
            </>
        </div>
    );
}

export default column;