import React from 'react';

const addNote = props => {
    return (
        <>
            <textarea
                ref={props.textareaRef}
                name="noteInput"
                id="noteInput"
                cols="30"
                rows="3"
                defaultValue=""
                placeholder="Write a note..."
                data-testid="note field" />
            <button onClick={props.onNotePostHandler}>Add note</button>
        </>
    );
};

export default addNote;
