import moment from 'moment';
const notes = [
    {
      id: 'EkMtaK-Sd',
      note: 'A note from yesterday.',
      tags: '#tag1 #tag2 #license:gpl',
      timestamp: moment()
        .startOf('day')
        .subtract(12, 'hours')
        .subtract(57, 'minutes')
    },
    {
      id: 'EkMtbK-Sd',
      note: 'Another note from yesterday.',
      tags: '#tag1 #tag2',
      timestamp: moment()
        .startOf('day')
        .subtract(10, 'hours')
        .subtract(40, 'minutes')
    },
    {
      id: 'EkMtcK-Sd',
      note: 'Note 3 from yesterday',
      tags: '#tag1 #tag2',
      timestamp: moment()
        .startOf('day')
        .subtract(10, 'hours')
        .subtract(17, 'minutes')
    },
    {
      id: 'ElMtcK-Sd',
      note: 'Note 4 from yesterday',
      tags: '#tag1 #tag2',
      timestamp: moment()
        .startOf('day')
        .subtract(4, 'hours')
        .subtract(18, 'minutes')
    },
    {
      id: 'EmMtcK-Sd',
      note: 'Note 5 from yesterday',
      tags: '#tag1 #tag2',
      timestamp: moment()
        .startOf('day')
        .subtract(4, 'hours')
        .subtract(17, 'minutes')
    },
    {
      id: 'EnMtcK-Sd',
      note: 'Note 6 from yesterday',
      tags: '#tag1 #tag2',
      timestamp: moment()
        .startOf('day')
        .subtract(4, 'hours')
        .subtract(16, 'minutes')
    },
    {
      id: 'EoMtcK-Sd',
      note: 'Note 7 from yesterday',
      tags: '#tag1 #tag2 #license:gpl',
      timestamp: moment()
        .startOf('day')
        .subtract(3, 'hours')
        .subtract(57, 'minutes')
    },
    {
      id: 'EpMtcK-Sd',
      note: 'Note 8 from yesterday',
      tags: '#tag1 #tag2',
      timestamp: moment()
        .startOf('day')
        .subtract(3, 'hours')
        .subtract(19, 'minutes')
    },
    {
      id: 'EqMtcK-Sd',
      note: 'Note 9 from yesterday',
      tags: '#tag1 #tag2',
      timestamp: moment()
        .startOf('day')
        .subtract(3, 'hours')
        .subtract(17, 'minutes')
    },
    {
      id: 'ErMtcK-Sd',
      note: 'Note 10 from yesterday',
      tags: '#tag1 #tag2 #license:gpl',
      timestamp: moment()
        .startOf('day')
        .subtract(3, 'hours')
    },
    {
      id: 'EsMtcK-Sd',
      note: 'Note 1 from today',
      tags: '#tag1 #tag2',
      timestamp: moment()
        .subtract(2, 'hours')
        .subtract(51, 'minutes')
    },
    {
      id: 'EtMtcK-Sd',
      note: 'Note 2 from today',
      tags: '#tag1 #tag2',
      timestamp: moment()
        .subtract(2, 'hours')
        .subtract(9, 'minutes')
    },
    {
      id: 'EuMtcK-Sd',
      note: 'Note 3 from today',
      tags: '#tag1 #awesome',
      timestamp: moment().subtract(22, 'minutes')
    },
    {
      id: 'EvMtcK-Sd',
      note: 'Note 4 from today',
      tags: '#tag1 #tag2',
      timestamp: moment().subtract(20, 'minutes')
    },
    {
      id: 'EwMtcK-Sd',
      note: 'Note 5 from today',
      tags: '#tag1 #tag2',
      timestamp: moment().subtract(19, 'minutes')
    },
    {
      id: 'EkU2ctZSO',
      note: 'Another  note',
      tags: '#tag1 #tag2',
      timestamp: moment().subtract(17, 'minutes')
    },
    {
      id: 'EkU2dtZSO',
      note: '# Heading\nA note with a heading',
      tags: '#tag1 #awesome #markdown',
      timestamp: moment()
    }
  ];

  export default notes;